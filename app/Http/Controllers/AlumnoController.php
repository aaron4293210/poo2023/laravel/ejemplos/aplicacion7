<?php

namespace App\Http\Controllers;

use App\Models\Alumno;
use Illuminate\Http\Request;

class AlumnoController extends Controller {
    /**
     * Display a listing of the resource.
    */
    public function index() {
        $alumnos = Alumno::all();

        return view('alumno.index', compact('alumnos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        return view('alumno.create');
    }

    /**
     * Store a newly created resource in storage.
    */
    public function store(Request $request) {
        $alumno = Alumno::create($request->all());

        return redirect()
            ->route('alumno.show', $alumno)
            ->with('mensaje', 'Alumno se ha creado con éxito')
        ;
    }

    /**
     * Display the specified resource.
    */
    public function show(Alumno $alumno) {
        return view('alumno.show', compact('alumno'));
    }

    /**
     * Show the form for editing the specified resource.
    */
    public function edit(Alumno $alumno) {
        return view('alumno.edit', compact('alumno'));
    }

    /**
     * Update the specified resource in storage.
    */
    public function update(Request $request, Alumno $alumno) {
        $alumno->update($request->all());

        return redirect()
            ->route('alumno.show', $alumno)
            ->with('mensaje', 'Alumno actualizado con éxito')
        ;
    }

    /**
     * Remove the specified resource from storage.
    */
    public function destroy(Alumno $alumno) {
        $alumno->delete();

        return redirect()
            ->route('alumno.index')
            ->with('mensaje', 'Alumno borrado con éxito')
        ;
    }
}
