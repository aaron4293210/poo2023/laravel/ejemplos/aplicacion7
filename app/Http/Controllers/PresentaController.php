<?php

namespace App\Http\Controllers;

use App\Models\Alumno;
use App\Models\Practica;
use App\Models\Presenta;
use Illuminate\Http\Request;

class PresentaController extends Controller {
    /**
     * Display a listing of the resource.
     */
    public function index() {
        $presentas = Presenta::all();

        return view('presenta.index', compact('presentas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        $practicas = Practica::all();
        $alumnos = Alumno::all();

        return view('presenta.create', compact('practicas', 'alumnos'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {
        $presenta = Presenta::create($request->all());

        return redirect()
            ->route('presenta.show', $presenta)
            ->with('mensaje', 'Presenta se ha creado con éxito')
        ;
    }

    /**
     * Display the specified resource.
     */
    public function show(Presenta $presentum) {;
        return view('presenta.show', compact('presentum'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Presenta $presentum) {
        $practicas = Practica::all();
        $alumnos = Alumno::all();

        return view('presenta.edit', compact('presentum', 'practicas', 'alumnos'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Presenta $presentum) {
        $presentum->update($request->all());

        return redirect()
            ->route('presenta.show', $presentum)
            ->with('mensaje', 'Presenta actualizado con éxito')
        ;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Presenta $presentum) {
        $presentum->delete();

        return redirect()
            ->route('presenta.index')
            ->with('mensaje', 'Presenta borrado con éxito')
        ;
    }

    public function confirmar(Presenta $presenta) {
        return view('presenta.confirmar', compact('presenta'));
    }
}
