<?php

namespace App\Http\Controllers;

use App\Models\Curso;
use App\Models\Practica;
use Illuminate\Http\Request;

class PracticaController extends Controller {
    /**
     * Display a listing of the resource.
     */
    public function index() {
        $practicas = Practica::all();

        return view('practica.index', compact('practicas'));
    }

    /**
     * Show the form for creating a new resource.
    */
    public function create() {
        $cursos = Curso::select('id', 'nombre')->get();

        return view('practica.create', compact('cursos'));
    }

    /**
     * Store a newly created resource in storage.
    */
    public function store(Request $request) {
        // $request->file('fichero')->store('practicas', 'public');

        // dd($request->all());


        $practica = Practica::create($request->all());

        return redirect()
            ->route('practica.show', $practica)
            ->with('mensaje', 'Curso se ha creado con éxito')
        ;
    }

    /**
     * Display the specified resource.
    */
    public function show(Practica $practica) {
        return view('practica.show', compact('practica'));
    }

    /**
     * Show the form for editing the specified resource.
    */
    public function edit(Practica $practica) {
        $cursos = Curso::select('id', 'nombre')->get();

        return view('practica.edit', compact('practica', 'cursos'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Practica $practica) {
        $practica->update($request->all());

        return redirect()
            ->route('practica.show', $practica)
            ->with('mensaje', 'Practica actualizado con éxito')
        ;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Practica $practica) {
        $practica->delete();

        return redirect()
            ->route('practica.index')
            ->with('mensaje', 'Practica borrado con éxito')
        ;
    }

    public function confirmar(Practica $practica) {
        return view('practica.confirmar', compact('practica'));
    }
}
