<?php

namespace App\Http\Controllers;

use App\Models\Alumno;
use App\Models\Curso;
use App\Models\Pertenece;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PerteneceController extends Controller {
    /**
     * Display a listing of the resource.
     */
    public function index() {
        $perteneces = Pertenece::all();

        return view('pertenece.index', compact('perteneces'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        $alumnos = Alumno::all();
        $cursos = Curso::select('id', 'nombre')->get();

        return view('pertenece.create', compact('alumnos', 'cursos'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {
        $pertenece = Pertenece::create($request->all());

        return redirect()
            ->route('pertenece.show', $pertenece)
            ->with('mensaje', 'Pertenece se ha creado con éxito')
        ;
    }

    /**
     * Display the specified resource.
     */
    public function show(Pertenece $pertenece) {
        return view('pertenece.show', compact('pertenece'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Pertenece $pertenece) {
        $alumnos = Alumno::all();
        $cursos = Curso::select('id', 'nombre')->get();

        return view('pertenece.edit', compact('pertenece', 'alumnos', 'cursos'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Pertenece $pertenece) {
        $pertenece->update($request->all());

        return redirect()
            ->route('pertenece.show', $pertenece)
            ->with('mensaje', 'Pertenece actualizado con éxito')
        ;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Pertenece $pertenece) {
        $pertenece->delete();

        return redirect()
            ->route('pertenece.index')
            ->with('mensaje', 'Pertenece borrado con éxito')
        ;
    }

    public function confirmar(Pertenece $pertenece) {
        return view('pertenece.confirmar', compact('pertenece'));
    }
}
