<?php

namespace App\Http\Controllers;

use App\Models\Curso;
use Illuminate\Http\Request;

class CursoController extends Controller {
    /**
     * Display a listing of the resource.
     */
    public function index() {
        $cursos = Curso::all();

        return view('curso.index', compact('cursos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        return view('curso.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {
        $curso = Curso::create($request->all());

        return redirect()
            ->route('curso.show', $curso)
            ->with('mensaje', 'Curso se ha creado con éxito')
        ;
    }

    /**
     * Display the specified resource.
    */
    public function show(Curso $curso) {
        return view('curso.show', compact('curso'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Curso $curso) {
        return view('curso.edit', compact('curso'));
    }

    /**
     * Update the specified resource in storage.
    */
    public function update(Request $request, Curso $curso) {
        $curso->update($request->all());

        return redirect()
            ->route('curso.show', $curso)
            ->with('mensaje', 'Curso actualizado con éxito')
        ;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Curso $curso) {
        $curso->delete();

        return redirect()
            ->route('curso.index')
            ->with('mensaje', 'Curso borrado con éxito')
        ;
    }
}
