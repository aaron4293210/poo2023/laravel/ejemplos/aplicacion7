<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;

class Curso extends Model {
    use HasFactory;

    protected $table = 'cursos';

    // Campos de asignación masiva
    protected $fillable = [
        'nombre',
        'duracion',
        'fechacomienzo',
        'observaciones',
    ];

    // Nombre de los campos  de las tablas
    public static $labels = [
        'id' => 'ID Curso',
        'nombre' => 'Nombre Curso',
        'duracion' => 'Duración Curso',
        'fechacomienzo' => 'Fecha Comienzo Curso',
        'observaciones' => 'Observaciones Curso',
    ];

    /**
     ** Recupera la lista de campos de la tabla asociada al modelo.
     *
     * @return array La lista de nombres de campos.
    */
    public function getFields(): array {
        return Schema::getColumnListing($this->table);
    }

    /**
     ** Recupera la etiqueta de un atributo dado.
     *
     * @param string $attribute El nombre del atributo.
     * @return string La etiqueta del atributo, o el nombre del atributo si no se encuentra una etiqueta.
    */
    public function getAttributeLabel($attribute): string {
        return self::$labels[$attribute] ?? $attribute;
    }

    /**
     ** Obtiene la lista de campos de la modelo de tabla asociada.
     *
     * @return array|HasMany La lista de nombres de campos asociados a Practica.
    */
    public function practicas() : HasMany {
        return $this->hasMany(Practica::class);
    }

    /**
     ** Obtiene la lista de campos de la modelo de tabla asociada.
     *
     * @return array|HasMany La lista de nombres de campos asociados a Pertenece.
    */
    public function perteneces() : HasMany {
        return $this->hasMany(Pertenece::class);
    }
}
