@extends('layouts.main')

@section('content')
    <h1>Editar Practica</h1>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p class="">{{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    @if ($errors->any())
        <div class="">
            <div class="" style="background-color: gray">
                <h2 class="">Error</h2>
                @foreach ($errors->all() as $error)
                    <p class=""> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <br><a href="{{ route('practica.index') }}" class="boton">Volver</a><br><br>

    <form action="{{ route('practica.update', $practica) }}" method="POST"  enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div>
            <label for="titulo">Titulo</label>
            <input type="text" name="titulo" id="titulo" value="{{ old('titulo', $practica->titulo) }}">
        </div><br>

        <div>
            <label for="fichero">Fichero</label>
            <input type="text" name="fichero" id="fichero" value="{{ old('fichero', $practica->fichero) }}">
        </div><br>

        <div>
            <label for="curso_id">Curso ID</label>

            <select name="curso_id" id="curso_id">
                @foreach ($cursos as $curso)
                    <option value="{{ $curso->id }}" {{ $practica->curso_id == $curso->id ? 'selected' : ''}} >{{ $curso->nombre }}</option>
                @endforeach
            </select>
        </div><br>

        <div>
            <button class="boton" type="submit">Actualizar</button>
        </div>
    </form><br><br>
@endsection
