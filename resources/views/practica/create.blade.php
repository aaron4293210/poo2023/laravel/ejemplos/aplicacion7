@extends('layouts.main')

@section('content')
    <h1>Crear Practica</h1>

    @if ($errors->any())
        <div class="">
            <div class="" style="">
                <h2 class="">Error</h2>

                @foreach ($errors->all() as $error)
                    <p class=""> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <form action="{{ route('practica.store') }}" method="POST"  enctype="multipart/form-data">
        @csrf

        <div>
            <label for="titulo">Titulo</label>
            <input type="text" name="titulo" id="titulo">
        </div><br>

        <div>
            <label for="fichero">Fichero</label>
            <input type="file" name="fichero" id="fichero">
        </div><br>

        <div>
            <label for="curso_id">Curso ID</label>

            <select name="curso_id" id="curso_id">
                <option value="">-- Seleccione --</option>
                @foreach ($cursos as $curso)
                    <option value="{{ $curso->id }}">{{ $curso->nombre }}</option>
                @endforeach
            </select>
        </div><br>

        <div>
            <button class="boton" type="submit">Crear</button>
        </div>
    </form><br>

    <a href="{{ route('practica.index') }}">Volver</a>
@endsection
