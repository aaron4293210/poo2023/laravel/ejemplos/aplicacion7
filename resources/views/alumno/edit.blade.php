@extends('layouts.main')

@section('content')
    <h1>Editar alumno</h1>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p class="">{{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    @if ($errors->any())
        <div class="">
            <div class="" style="background-color: gray">
                <h2 class="">Error</h2>
                @foreach ($errors->all() as $error)
                    <p class=""> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <a href="{{ route('alumno.index') }}" class="boton">Volver</a>

    <form action="{{ route('alumno.update', $alumno) }}" method="POST"  enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre" value="{{ old('nombre', $alumno->nombre) }}">
        </div>

        <div>
            <label for="apellidos">Apellidos</label>
            <input type="text" name="apellidos" id="apellidos" value="{{ old('apellidos', $alumno->apellidos) }}">
        </div>

        <div>
            <label for="fechanacimiento">Fecha de Nacimiento</label>
            <input type="date" name="fechanacimiento" id="fechanacimiento" value="{{ old('fechanacimiento', $alumno->fechanacimiento) }}">
        </div>

        <div>
            <label for="email">Email</label>
            <input type="email" name="email" id="email" value="{{ old('email', $alumno->email) }}">
        </div>

        <div>
            <label for="foto">Foto</label>
            <input type="text" name="foto" id="foto" value="{{ old('foto', $alumno->foto) }}">
        </div>

        <div>
            <button class="boton" type="submit">Actualizar</button>
        </div>
    </form>
@endsection
