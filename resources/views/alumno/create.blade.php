@extends('layouts.main')

@section('content')
    <h1>Crear Alumno</h1>

    @if ($errors->any())
        <div class="">
            <div class="" style="">
                <h2 class="">Error</h2>

                @foreach ($errors->all() as $error)
                    <p class=""> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <a href="{{ route('alumno.index') }}" class="boton">Volver</a>

    <form action="{{ route('alumno.store') }}" method="POST"  enctype="multipart/form-data">
        @csrf

        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre">
        </div>

        <div>
            <label for="apellidos">Apellidos</label>
            <input type="text" name="apellidos" id="apellidos">
        </div>

        <div>
            <label for="fechanacimiento">Fecha de Nacimiento</label>
            <input type="date" name="fechanacimiento" id="fechanacimiento">
        </div>

        <div>
            <label for="email">Email</label>
            <input type="email" name="email" id="email">
        </div>

        <div>
            <label for="foto">Foto</label>
            <input type="text" name="foto" id="foto">
        </div>

        <div>
            <button class="boton" type="submit">Crear</button>
        </div>
    </form>
@endsection
