<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inicio</title>
    @vite('resources/scss/app.scss')
</head>
<body>
    <div class="menu">
        <ul>
            <li><a href="{{ route('inicio') }}" class="{{ request()->routeIs('inicio') ? 'active' : '' }}">Inicio</a></li>
            <li><a href="{{ route('alumno.index') }} " class="{{ request()->routeIs('alumno.*') ? 'active' : '' }}">Alumnos</a></li>
            <li><a href="{{ route('curso.index') }}" class="{{ request()->routeIs('curso.*') ? 'active' : '' }}">Cursos</a></li>
            <li><a href="{{ route('practica.index') }}" class="{{ request()->routeIs('practica.*') ? 'active' : '' }}">Practicas</a></li>
            <li><a href="{{ route('pertenece.index') }}" class="{{ request()->routeIs('pertenece.*') ? 'active' : '' }}">Perteneces</a></li>
            <li><a href="{{ route('presenta.index') }}" class="{{ request()->routeIs('presenta.*') ? 'active' : '' }}">Presentas</a></li>
        </ul>
    </div>

    <div>
        @yield('content')
    </div>

    <br>
    <div class="footer">
        <p>Aplicación de gestion de practicas y alumnos con laravel</p>
    </div>
    @vite('resources/js/app.js')
</body>
</html>
