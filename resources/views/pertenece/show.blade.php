@extends('layouts.main')

@section('content')
    <h1>Listado Pertenece</h1>

    <div>
        <a href="{{ route('pertenece.index') }}" class="boton">Volver</a><br><br>
    </div>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    <div class="listado">
        <div class="tarjeta">
            <ul>
                <li>ID: {{ $pertenece->id }}</li>
                <li>ID Alumno: {{ $pertenece->alumno_id }} - {{ $pertenece->alumno->nombre }}</li>
                <li>Curso ID: {{ $pertenece->curso_id }} - {{ $pertenece->curso->nombre }}</li>
            </ul>

            <div class="botones">
                <a class="boton" href="{{ route('pertenece.edit', $pertenece) }}">Editar</a>

                <form action="{{ route('pertenece.destroy', $pertenece)}}" method="POST" id="eliminar">
                    @csrf @method('DELETE')
                    <button class="boton" type="submit">Borrar</button>
                </form>

                <a href="{{ route('pertenece.confirmar', $pertenece) }}" class="boton"> Eliminar 2 </a>
            </div>
        </div>
    </div>
@endsection
