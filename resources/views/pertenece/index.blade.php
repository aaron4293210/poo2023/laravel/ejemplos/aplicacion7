@extends('layouts.main')

@section('content')
    <h1>Listado Perteneces</h1>

    <div>
        <a href="{{ route('pertenece.create') }}" class="boton">Crear Pertenece</a><br><br>
    </div>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    <div class="listado">
        @foreach ($perteneces as $pertenece)
            <div class="tarjeta">
                <ul>
                    <li><a href="{{ route('pertenece.show', $pertenece) }}" >ID:</a> {{ $pertenece->id }}</li>
                    <li>Alumno ID: {{ $pertenece->alumno_id }} - {{ $pertenece->alumno->nombre }}</li>
                    <li>Curso ID: {{ $pertenece->curso_id }} - {{ $pertenece->curso->nombre }}</li>
                </ul>

                <div class="botones">
                    <a href="{{ route('pertenece.show', $pertenece) }}" class="boton">Ver</a>
                    <a href="{{ route('pertenece.edit', $pertenece) }}" class="boton">Editar</a>

                    <form action="{{ route('pertenece.destroy', $pertenece)}}" method="POST" id="eliminar">
                        @csrf @method('DELETE')
                        <button class="boton" type="submit">Borrar</button>
                    </form>

                    <a href="{{ route('pertenece.confirmar', $pertenece) }}" class="boton"> Eliminar 2 </a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
