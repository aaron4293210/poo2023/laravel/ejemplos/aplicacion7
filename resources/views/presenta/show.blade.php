@extends('layouts.main')

@section('content')
    <h1>Listado Presenta</h1>

    <div>
        <a href="{{ route('presenta.index') }}" class="boton">Volver</a><br><br>
    </div>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    <div class="tarjeta">
        <ul>
            <li>ID: {{ $presentum->id }}</li>
            <li>Nota: {{ $presentum->nota }}</li>
            <li>Practica ID: {{ $presentum->practica_id }} - {{ $presentum->practica->titulo }}</li>
            <li>Alumno ID: {{ $presentum->alumno_id }} - {{ $presentum->alumno->nombre }}</li>
        </ul>

        <div class="botones">
            <a class="boton" href="{{ route('presenta.edit', $presentum) }}">Editar</a>

            <form action="{{ route('presenta.destroy', $presentum)}}" method="POST" id="eliminar">
                @csrf @method('DELETE')
                <button class="boton" type="submit">Borrar</button>
            </form>

            {{-- <a href="{{ route('presenta.confirmar', $presenta) }}" class="boton"> Eliminar 2 </a> --}}
        </div>
    </div>
@endsection
