@extends('layouts.main')

@section('content')
    <h1>Crear Presenta</h1>

    @if ($errors->any())
        <div class="">
            <div class="" style="">
                <h2 class="">Error</h2>

                @foreach ($errors->all() as $error)
                    <p class=""> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <a href="{{ route('presenta.index') }}" class="boton">Volver</a>

    <form action="{{ route('presenta.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div>
            <label for="nota">Nota</label>
            <input type="number" name="nota" id="nota">
        </div>

        <div>
            <label for="practica_id">Practica ID</label>

            <select name="practica_id" id="practica_id">
                <option value="">-- Seleccione --</option>

                @foreach ($practicas as $practica)
                    <option value="{{ $practica->id }}">{{ $practica->titulo }}</option>
                @endforeach
            </select>
        </div>

        <div>
            <label for="alumno_id">Alumno ID</label>

            <select name="alumno_id" id="alumno_id">
                <option value="">-- Seleccione --</option>

                @foreach ($alumnos as $alumno)
                    <option value="{{ $alumno->id }}">{{ $alumno->nombrecompleto }}</option>
                @endforeach
            </select>
        </div>

        <div>
            <button class="boton" type="submit">Crear</button>
        </div>
    </form>
@endsection
