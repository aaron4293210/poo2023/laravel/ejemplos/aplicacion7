@extends('layouts.main')

@section('content')
    <h1>Editar Presenta</h1>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p class="">{{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    @if ($errors->any())
        <div class="">
            <div class="" style="background-color: gray">
                <h2 class="">Error</h2>
                @foreach ($errors->all() as $error)
                    <p class=""> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <br><a href="{{ route('presenta.index') }}" class="boton">Volver</a><br>


    <form action="{{ route('presenta.update', $presentum) }}" method="POST"  enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div>
            <label for="nota">Nota</label>
            <input type="number" name="nota" id="nota" value="{{ old('nota', $presentum->nota) }}">
        </div>

        <div>
            <label for="practica_id">Practica ID</label>

            <select name="practica_id" id="practica_id">
                <option value="">-- Seleccione --</option>

                @foreach ($practicas as $practica)
                    <option value="{{ $practica->id }}" {{ $presentum->practica_id == $practica->id ? 'selected' : '' }}>{{ $practica->titulo }}</option>
                @endforeach
            </select>
        </div>

        <div>
            <label for="alumno_id">Alumno ID</label>

            <select name="alumno_id" id="alumno_id">
                <option value="">-- Seleccione --</option>

                @foreach ($alumnos as $alumno)
                    <option value="{{ $alumno->id }}" {{ $presentum->alumno_id == $alumno->id ? 'selected' : '' }}>{{ $alumno->nombrecompleto }}</option>
                @endforeach
            </select>
        </div>

        <div>
            <button class="boton" type="submit">Actualizar</button>
        </div>
    </form>
@endsection
