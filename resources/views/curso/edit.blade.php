@extends('layouts.main')

@section('content')
    <h1>Editar Curso</h1>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p class="">{{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    @if ($errors->any())
        <div class="">
            <div class="" style="background-color: gray">
                <h2 class="">Error</h2>
                @foreach ($errors->all() as $error)
                    <p class=""> {{ $error }} </p>
                @endforeach
            </div>
        </div>
    @endif

    <br><a href="{{ route('curso.index') }}" class="boton">Volver</a>

    <form action="{{ route('curso.update', $curso) }}" method="POST"  enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre" value="{{ old('nombre', $curso->nombre) }}">
        </div>

        <div>
            <label for="duracion">Duracion</label>
            <input type="number" name="duracion" id="duracion" value="{{ old('duracion', $curso->duracion) }}">
        </div>

        <div>
            <label for="fechacomienzo">Fecha de Comienzo</label>
            <input type="date" name="fechacomienzo" id="fechacomienzo" value="{{ old('fechacomienzo', $curso->fechacomienzo) }}">
        </div>

        <div>
            <label for="observaciones">Observaciones</label>
            <input type="text" name="observaciones" id="observaciones" value="{{ old('observaciones', $curso->observaciones) }}">
        </div>

        <div>
            <button class="boton" type="submit">Actualizar</button>
        </div>
    </form>
@endsection
