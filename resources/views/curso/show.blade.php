@extends('layouts.main')

@section('content')
    <h1>Listado Cursos</h1>

    <div>
        <a href="{{ route('curso.index') }}" class="boton">Volver</a><br><br>
    </div>

    @if (session('mensaje'))
        <div class="">
            <div class="" style="background-color: gray">
                <p> {{ session('mensaje') }} </p>
            </div>
        </div>
    @endif

    <div class="tarjeta">
        <ul>
            <li>ID: {{ $curso->id }}</li>
            <li>Nombre: {{ $curso->nombre }}</li>
            <li>Duracion: {{ $curso->duracion }}</li>
            <li>Fecha Comienzo: {{ $curso->fechacomienzo }}</li>
            <li>Observaciones: {{ $curso->observaciones }}</li>
        </ul>

        <div class="botones">
            <a class="boton" href="{{ route('curso.edit', $curso) }}">Editar</a>

            <form action="{{ route('curso.destroy', $curso)}}" method="POST" id="eliminar">
                @csrf @method('DELETE')
                <button type="submit">Borrar</button>
            </form><br><br>
        </div>
    </div>
@endsection
