<?php

namespace Database\Seeders;

use App\Models\Alumno;
use App\Models\Curso;
use App\Models\Practica;
use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     */
    public function run(): void {
        // Alumno::factory(10)->create();
        // Curso::factory(10)->create();

        // for ($i = 0; $i < 10; $i++) {
        //     $curso = Curso::factory()->create();

        //     Practica::factory()
        //         ->for($curso)
        //         ->create();
        // }

        $this->call([
            AlumnoSeeder::class,
            CursoSeeder::class,
            PracticaSeeder::class,
            PresentaSeeder::class,
            PerteneceSeeder::class
        ]);
    }
}
